<?php
$on = new login;
$start = $on->search_user();
class login {
    public function __construct() {
        include 'config.php';
        try{
            $this->BBDD = new PDO(PDO_HOSTNAME, PDO_USER, PDO_PASS);
            $this->BBDD->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->BBDD->exec(PDO_CHAR);
        } catch (PDOException $ex) {
            die("Error al intentar conectar con la base de datos" . $ex->getLine() . PHP_EOL . $ex->getCode() . " " . PHP_EOL. $ex->getMessage());
        }
    }
    public function search_user(){
        try{
            $mail_check = $this->comprobar_email($_POST['log']);
            if($mail_check==1){
                $jsondata = array();
                //LOGIN WITH EMAIL
                $query = htmlentities(addslashes($_POST['log']));
                $mail_login = $this->BBDD->prepare("SELECT * FROM usuarios WHERE email LIKE ? ");
                $mail_login->execute(array(htmlentities(addslashes("%$query%"))));
                $identity = $mail_login->fetchAll(PDO::FETCH_OBJ);
                foreach($identity as $obj){
                   /* echo $obj->id . "<br />";                  
                    echo $obj->email . "<br />";
                    echo $obj->password . "<br />";*/
                }
                $row_count = $mail_login->rowCount();
                if($row_count==0){
                    $jsondata["user_unknow"] = "UNKNOW";
                    echo json_encode($jsondata);                   
                    die();
                }
               $hash = $obj->password;
               if(password_verify(htmlentities(addslashes($_POST['pwd'])), $hash)){             
                   if($row_count==1){
                       session_start();
                       $_SESSION['user'] = $_POST['log'];
                       $_SESSION['nick'] = $obj->usuario;
                       $_SESSION['id'] = $obj->id;                       
                       $_SESSION['permise'] = "Accepted";                
                       $_SESSION['random'] = $this->generateRandomString(50); 
                       // ajax response
                       $jsondata['user_name'] = $_SESSION['user'];
                       $jsondata['user_id'] = $_SESSION['id'];
                       $jsondata['data'] = $_SESSION['random'];
                       $jsondata['permission'] = $_SESSION['permise'];   
                       $jsondata['done'] = "done";
                       header('Content-Type: application/json');
                       echo json_encode($jsondata);
                       
                       //echo $_SESSION['user'];
                   }
               }else{
                   
                   $jsondata['pw_wrong'] = "FAIL";
                   echo json_encode($jsondata);
                   die();
                  
               }
            }else{
                //echo "USER";
                //LOGIN WITH USER
                $user_login = $this->BBDD->prepare("SELECT usuario,password,id FROM usuarios WHERE usuario = ? ");             
                $user_login->execute(array(htmlentities(addslashes($_POST['log']))));
                $identity = $user_login->fetchAll(PDO::FETCH_OBJ);
                foreach($identity as $obj){
                    /*echo $obj->usuario . "<br />";                  
                    echo $obj->id . "<br />";
                    echo $obj->password . "<br />";*/
                }
                $row_count = $user_login->rowCount();
                if($row_count==0){
                    
                    $jsondata["user_unknow"] = "UNKNOW";
                    echo json_encode($jsondata);                   
                    die();
                    
                }
                $hash = $obj->password;
               if(password_verify($_POST['pwd'], $hash)){                 
                   if($row_count==1){
                       session_start();
                       $_SESSION['user'] = $_POST['log'];
                       $_SESSION['nick'] = $obj->usuario;
                       $_SESSION['id'] = $obj->id;
                       $_SESSION['permise'] = "Accepted";
                       $_SESSION['random'] = $this->generateRandomString(50); 
                       // ajax response
                       $jsondata['user_name'] = $_SESSION['user'];
                       $jsondata['user_id'] = $_SESSION['id'];
                       $jsondata['data'] = $_SESSION['random'];
                       $jsondata['permission'] = $_SESSION['permise'];
                       $jsondata['done'] = "done";
                       header('Content-Type: application/json');
                       echo json_encode($jsondata);
                       
                       //echo $_SESSION['user'];
                   }
               }else{
                   
                   $jsondata['pw_wrong'] = "FAIL";
                   echo json_encode($jsondata);
                   die();
                  
               }
            }
        } catch (PDOException $ex) {
            die("Error al intentar buscar el usuario en la base de datos" . PHP_EOL . " " . $ex->getMessage() . PHP_EOL . " " . $ex->getLine()
                    . PHP_EOL . " " . $ex->getCode());
        }         
       }
        public function generateRandomString($length) { 
             return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length); 
                }
                function comprobar_email($email){ 
   	$mail_correcto = 0; 
   	if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
      	if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) {         	 
         	if (substr_count($email,".")>= 1){            	
            	$term_dom = substr(strrchr ($email, '.'),1); 
            	if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){ 
               	$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
               	$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
               	if ($caracter_ult != "@" && $caracter_ult != "."){ 
                  	$mail_correcto = 1; 
               	} 
            } 
          } 
      	} 
   } 
   	if ($mail_correcto) 
      	return 1; 
   	else 
      	return 0; 
}
    private $BBDD;
}
